import 'dart:convert';
import 'dart:math';
import 'package:flame/gestures.dart';
import 'package:flame/game.dart';
import 'package:flame/palette.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import 'board.dart';
import 'colors.dart';

void main() {
  final myGame = GameOfLifeDeatchmatch();
  runApp(
    GameWidget(
      game: myGame,
    ),
  );
}

/// Our horrible hardcoded server address
const server = "83.136.249.41:8989";
// const server = "localhost:8989";

class GameOfLifeDeatchmatch extends Game with TapDetector {
  static const topMargin = 80;
  final baseFontConfig = TextPaintConfig(
    fontSize: 36.0,
    color: BasicPalette.magenta.color,
    fontFamily: 'Awesome Font',
  );
  Board board = Board();
  late final WebSocketChannel _channel;
  late TextPaint playerNumberTextPaint;
  late TextPaint topScoreTextPaint;
  var rectSize = 10.0;
  int myPlayerNumber = 0;
  int myCellsLeft = 0;

  @override
  Future<void> onLoad() async {
    rectSize = min(size.x, size.y) / board.size;
    playerNumberTextPaint = createPlayerText(BasicPalette.magenta.color);
    topScoreTextPaint =
        playerNumberTextPaint.copyWith((tpc) => tpc.withFontSize(16));

    print("On load called. Connecting to socket server!");
    _channel = WebSocketChannel.connect(
      Uri.parse('ws://$server/ws'),
    );
    _channel.stream.listen(onServerMessage);
    _channel.sink.add(json.encode({"message": "moi"}));
  }

  TextPaint createPlayerText(Color col) {
    return TextPaint(config: baseFontConfig.withColor(col));
  }

  void onServerMessage(msg) {
    print("received message from server");
    var content = json.decode(msg);
    if (content.containsKey("board")) {
      board.setFromArray(content["board"]);
    }
    if (content.containsKey("setPlayerNumber")) {
      myPlayerNumber = content["setPlayerNumber"];
      playerNumberTextPaint =
          createPlayerText(playerColors[myPlayerNumber].color);
    }
    if (content.containsKey("setPlayerCellsLeft")) {
      myCellsLeft = content["setPlayerCellsLeft"];
    }
  }

  @override
  void onTapDown(TapDownInfo event) {
    var pos = event.eventPosition.game.toOffset();
    var cellX = (pos.dx / rectSize).floor();
    var cellY = ((pos.dy - topMargin) / rectSize).floor();
    print("${pos.dx}, ${pos.dy} == ${cellX}, ${cellY}");
    _channel.sink.add(json.encode({
      "tap": [cellX, cellY]
    }));
  }

  @override
  void update(double dt) {}

  @override
  void render(Canvas canvas) {
    var x = 0.0, y = 0.0;
    var topList = {};
    for (var list in board.cells) {
      for (var cell in list) {
        if (cell.alive) {
          canvas.drawRect(
              Rect.fromLTWH(
                  x * rectSize, topMargin + y * rectSize, rectSize, rectSize),
              playerColors[cell.value].paint());

          if (topList.containsKey(cell.value)) {
            topList[cell.value] += 1;
          } else {
            topList[cell.value] = 1;
          }
        }
        x++;
      }
      y++;
      x = 0;
    }
    renderTopbar(canvas, topList);
  }

  void renderTopbar(canvas, topList) {
    var myScore = topList[myPlayerNumber] ?? 0;
    String text = "Player $myPlayerNumber ($myCellsLeft): $myScore";
    playerNumberTextPaint.render(canvas, text, Vector2(10, 5));

    var sorted = topList.values.toList()..sort();
    var reversed = sorted.reversed.toList();
    for (var i = 0; i < 3; i++) {
      if (reversed.length > i) {
        var score = reversed[i];
        var entry =
            topList.keys.toList().firstWhere((key) => topList[key] == score);

        var text = entry == 1
            ? "Computer: ${topList[entry]}"
            : "Player $entry: ${topList[entry]}";
        var personalColorTextPaint = topScoreTextPaint
            .copyWith((tpc) => tpc.withColor(playerColors[entry].color));
        personalColorTextPaint.render(
            canvas, text, Vector2(size.x - text.length * 10, 5 + 20.0 * i));
      }
    }
  }

  // @override
  // void dispose() {
  //   _channel.sink.close();
  //   super.dispose();
  // }
}
