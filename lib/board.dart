import 'dart:math';

const maxPlayers = 40;
const playerMaxCells = 5;

/// This could contain more stuff (but for now it does not).
class Player {
  int number;
  int cellsLeft = playerMaxCells;
  Player(this.number);
}

/// Action initiated by player. Only action right now is to drop some live
/// cells at x, y
class PlayerAction {
  Player player;
  int x;
  int y;
  PlayerAction(this.player, this.x, this.y);
}

/// One cell on the size x size Board
class Cell {
  int age = 0;
  int value = 0;
  Cell(this.value);

  bool get alive => value > 0;
}

/// The main structure for the Game. Contains the game logic.
class Board {
  int size = 100;
  var random = Random();

  late List<PlayerAction> actions;
  late List<List<Cell>> cells;
  late List<int> transmitArray;
  late List<Player> players;

  Board() {
    cells = List<List<Cell>>.generate(
        size,
        (i) => List<Cell>.generate(
            size, (i) => random.nextBool() ? Cell(1) : Cell(0)));
    transmitArray = List.generate(size * size, (i) => 0);
    players = List.generate(maxPlayers, (i) => Player(i));
    actions = [];
  }

  /// Plays one round of Game of Life with some special considerations to
  /// our multiplayer setup
  void updateCells(Board fromBoard) {
    cells.asMap().forEach((y, list) {
      list.asMap().forEach((x, _cell) {
        _updateCell(fromBoard, x, y);
      });
    });
  }

  /// reimburse the cells the player can use
  List<int> givePlayersCells() {
    List<int> updatedPlayerNumbers = [];
    for (var player in players) {
      if (player.cellsLeft < playerMaxCells) {
        player.cellsLeft++;
        updatedPlayerNumbers.add(player.number);
      }
    }
    return updatedPlayerNumbers;
  }

  Cell _updateCell(Board fromBoard, x, y) {
    List<Cell> neighbours = getNeighbours(fromBoard, x, y);
    int n = neighbours.length;
    // https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
    // Any live cell with fewer than two live neighbours dies, as if by underpopulation.
    // Any live cell with two or three live neighbours lives on to the next generation.
    // Any live cell with more than three live neighbours dies, as if by overpopulation.
    // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    // => converts to ...
    Cell fromCell = fromBoard.at(x, y);
    Cell toCell = at(x, y);
    if (fromCell.alive) {
      if (n > 1 && n < 4) {
        toCell.age = fromCell.age + 1;
        toCell.value = fromCell.value;
        return toCell;
      }
    } else {
      if (n == 3) {
        int winner = getBiggestNeighbour(neighbours);
        toCell.age = 0;
        toCell.value = winner;
        return toCell;
      }
    }
    toCell.age = 0;
    toCell.value = 0;
    return toCell;
  }

  void debugPrint() {
    var aliveCount = 0;
    var avgAge = 0;
    cells.asMap().forEach((y, list) {
      list.asMap().forEach((x, cell) {
        if (cell.alive) {
          aliveCount += 1;
          avgAge += cell.age;
        }
      });
    });
    print("debug: $aliveCount cells alive, avg age=${avgAge / aliveCount}");
  }

  Cell at(x, y) {
    return cells[y][x];
  }

  void turnAliveAt(x, y, player) {
    for (var i = 0; i < 8; i++) {
      var randX = x + random.nextInt(4) - random.nextInt(4);
      var randY = y + random.nextInt(4) - random.nextInt(4);
      randX = randX.clamp(0, size - 1);
      randY = randY.clamp(0, size - 1);
      Cell cell = cells[randY][randX];
      cell.age = 0;
      cell.value = player.number;
    }
  }

  /// Add an action coming from a player to be executed during the next update
  int addPlayerAction(Player player, x, y) {
    if (player.cellsLeft > 0) {
      actions.add(PlayerAction(player, x, y));
      player.cellsLeft--;
    }
    return player.cellsLeft;
  }

  /// Go through our saved player actions and execute them
  void executePlayerActions() {
    for (var action in actions) {
      turnAliveAt(action.x, action.y, action.player);
    }
    actions.clear();
  }

  /// Get a list of neighbouring alive cells
  List<Cell> getNeighbours(Board fromBoard, x, y) {
    List<Cell> neighbours = [];
    // check top and bottom and same row
    for (var i = -1; i < 2; i++) {
      var xPos = x + i;
      if (xPos >= 0 && xPos < (size - 1)) {
        if (y > 0) {
          var above = fromBoard.at(xPos, y - 1);
          if (above.alive) neighbours.add(above);
        }
        if (y < (size - 1)) {
          var below = fromBoard.at(xPos, y + 1);
          if (below.alive) neighbours.add(below);
        }
        if (i != 0) {
          var side = fromBoard.at(xPos, y);
          if (side.alive) neighbours.add(side);
        }
      }
    }
    return neighbours;
  }

  /// Get the most populous neighbour from the list of neighbours
  int getBiggestNeighbour(List<Cell> neighbours) {
    var counter = {};

    for (var neighbour in neighbours) {
      final val = neighbour.value;
      if (!counter.containsKey(val)) {
        counter[val] = 1;
      } else {
        counter[val] += 1;
      }
    }
    var biggest = counter.values.toList()..sort((a, b) => b.compareTo(a));
    var biggestKey =
        counter.keys.firstWhere((k) => counter[k] == biggest.first);
    return biggestKey;
  }

  /// Set the contents of our board directly from array, discarding any
  /// age information (used by clients)
  void setFromArray(List<dynamic> array) {
    cells.asMap().forEach((y, list) {
      list.asMap().forEach((x, _cell) {
        var cell = list[x];
        var arrValue = array[x + y * size];
        cell.value = arrValue;
        cell.age = 0;
      });
    });
  }

  /// Throw our board into a simple array (used by the server)
  List<int> toArray() {
    cells.asMap().forEach((y, list) {
      list.asMap().forEach((x, _cell) {
        transmitArray[x + y * size] = list[x].value;
      });
    });
    return transmitArray;
  }
}
