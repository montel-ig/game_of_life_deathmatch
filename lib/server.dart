import 'board.dart';
import 'dart:io' show HttpServer, HttpRequest, WebSocket, WebSocketTransformer;
import 'dart:convert' show json;
import 'dart:async' show Timer;
import 'dart:io';

class PlayerSocket {
  Player player;
  WebSocket connection;
  PlayerSocket(this.player, this.connection);
}

class Server {
  Board board1 = Board();
  Board board2 = Board();
  late List<PlayerSocket> playerConnections;
  late Board curBoard;
  var dtCounter = 0.0;
  static const refreshRate = Duration(milliseconds: 100);
  static const buildBasePath = "../build/web";
  int prevBoardUpdate = 0;
  int prevPlayerUpdate = 0;

  Server() {
    curBoard = board1;
    playerConnections = [];
    HttpServer.bind('0.0.0.0', 8989).then((HttpServer server) {
      print('[+]HttpServer listening at -- http://0.0.0.0:8989');

      server.listen((HttpRequest request) async {
        if (!request.uri.path.startsWith('/ws')) {
          request.response.statusCode = HttpStatus.notFound;
          request.response.write('Not found');
          request.response.close();
        } else {
          WebSocketTransformer.upgrade(request).then((WebSocket ws) {
            var playerNumber = playerConnections.length + 2;
            var newPlayerConnection =
                PlayerSocket(curBoard.players[playerNumber], ws);
            playerConnections.add(newPlayerConnection);
            ws.add(json.encode({'setPlayerNumber': playerNumber}));
            ws.add(json.encode({
              'setPlayerCellsLeft': curBoard.players[playerNumber].cellsLeft
            }));
            ws.listen(
              (data) => onClientData(data, ws, request, newPlayerConnection),
              onDone: () => print('[+]Done :)'),
              onError: (err) => print('[!]Error -- ${err.toString()}'),
              cancelOnError: true,
            );
          }, onError: (err) => print('[!]Error -- ${err.toString()}'));
        }
      }, onError: (err) => print('[!]Error -- ${err.toString()}'));
    }, onError: (err) => print('[!]Error -- ${err.toString()}'));
    updateLoop(DateTime.now().millisecondsSinceEpoch);
  }

  void onClientData(data, ws, request, playerConnection) {
    print(
        'From player ${playerConnection.player.number} ${request.connectionInfo?.remoteAddress} -- $data');
    var content = json.decode(data);
    if (content.containsKey('tap')) {
      // curBoard.setAt(content['tap'][0], content['tap'][1], true);
      int cellsLeft = curBoard.addPlayerAction(
          playerConnection.player, content['tap'][0], content['tap'][1]);
      ws.add(json.encode({"setPlayerCellsLeft": cellsLeft}));
    }

    // reply with the current state
    // if (ws.readyState == WebSocket.open) {
    //   ws.add(json.encode({'board': curBoard.toArray()}));
    // }
  }

  void updateLoop(time) {
    //print(".");
    if (time - prevBoardUpdate > 500) {
      var nextBoard = curBoard == board1 ? board2 : board1;
      curBoard.executePlayerActions();
      nextBoard.updateCells(curBoard);
      curBoard = nextBoard;
      broadcastBoardToPlayers();
      prevBoardUpdate = time;
    }
    if (time - prevPlayerUpdate > 1500) {
      var updatedPlayerNumbers = curBoard.givePlayersCells();
      broadcastCellCountToPlayers(updatedPlayerNumbers);
      prevPlayerUpdate = time;
      curBoard.debugPrint();
    }
    Timer(refreshRate, () {
      updateLoop(DateTime.now().millisecondsSinceEpoch);
    });
  }

  void broadcastBoardToPlayers() {
    final boardStateJson = json.encode({'board': curBoard.toArray()});
    for (PlayerSocket playerConnection in playerConnections) {
      if (playerConnection.connection.readyState == WebSocket.open) {
        playerConnection.connection.add(boardStateJson);
      }
    }
  }

  void broadcastCellCountToPlayers(List<int> updatedPlayerNumbers) {
    for (PlayerSocket playerConnection in playerConnections) {
      if (updatedPlayerNumbers.contains(playerConnection.player.number)) {
        if (playerConnection.connection.readyState == WebSocket.open) {
          final cellsCountJson = json.encode(
              {'setPlayerCellsLeft': playerConnection.player.cellsLeft});
          playerConnection.connection.add(cellsCountJson);
        }
      }
    }
  }
}

void main() {
  Server();
}
