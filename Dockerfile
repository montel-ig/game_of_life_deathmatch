FROM dart:2.14
LABEL author="lauri@montel.fi"

WORKDIR /app

COPY lib /app/

CMD [ "dart", "server.dart" ]