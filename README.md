# Game of Life Deathmatch

The competitive version of Conway's Game of Life. Features a Flutter web frontend and a server implemented in Dart.

## The server

Runs with:
```
cd lib
dart server.dart
```
Will listen on 0.0.0.0:8999.

## The mobile app

Run with your flutter setup. Will attempt to connect to the server.
